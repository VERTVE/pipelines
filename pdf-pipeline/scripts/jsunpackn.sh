#!/bin/bash
SAMPLESPATH=$(pwd)
cd /jsunpack-n

# If folder is empty, do nothing. Otherwise loop through samples.
if [[ "$(ls $SAMPLESPATH/sample-source |wc -l)" == 0 ]]; then
    	echo "Folder is empty"
        echo "$(date)" | tee -a $SAMPLESPATH/output-files/jsunpackn-empty.log
    else
    	# Loop through all files in the sample folder
    	for file in $SAMPLESPATH/sample-source/*
	    do
	    xbase=${file##*/};xfext=${xbase##*.};xpref=${xbase%.*}
	    echo "Processing ${xpref}.${xfext}"
	    /usr/bin/python jsunpackn.py $file -d $SAMPLESPATH/output-files/shellcode/${xpref}.${xfext}/ | tee -a $SAMPLESPATH/output-files/jsunpack-n.log
	    echo "output folder: $SAMPLESPATH/output-files/shellcode/${xpref}.${xfext}/"

	    # Remove sample from work folder after analysis
	    rm $file
	    done
fi
