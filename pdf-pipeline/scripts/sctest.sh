#!/bin/bash
OUTPUTPATH=$(pwd)/output-files/
ORIGSAMPLESPATH=$(pwd)

# Do nothing if folder is empty. Otherwise loop through samples.
if [[ "$(ls $ORIGSAMPLESPATH/results/results/shellcode |wc -l)" == 0 ]]; then
	echo "Folder is empty"
    echo $(date) > $OUTPUTPATH/sctest-empty.log
else
	for folder in $ORIGSAMPLESPATH/results/results/shellcode/*/
	  do
	    printf "Working on folder: "${folder}"\n" >> $OUTPUTPATH/sctest.log

	    SAMPLESPATH=${folder%?}
	    cd /peepdf

	    for file in $SAMPLESPATH/*
	        do
	        xbase=${file##*/}; xfext=${xbase##*.}; xpref=${xbase%.*}

		echo $(basename $SAMPLESPATH)/$xbase Results: >> $OUTPUTPATH/sctest.log
		/usr/bin/python peepdf.py $file -f --command="sctest file ${file}" >> $OUTPUTPATH/sctest.log
	    done
	done

    cat $OUTPUTPATH/sctest.log
    rm -rf $OUTPUTPATH/sample
fi
