#!/bin/sh
OUTPUT_FILES=$(pwd)

if [ ! -d "output-files/identify-compiler" ]; then mkdir output-files/identify-compiler; fi

# Create temporary folders for sorting the samples
mkdir temp-sort-files/bin-python temp-sort-files/bin-other

for file in sample-source/*; do
    if [ ! -d "$file" ]; then
        echo Analysing: "${file##*/}"
	hash=$(sha256sum "${file}" | cut -d ' ' -f1)

        VERSION=$(python3 /python-exe-analysis/exe_python_version.py "$file")
	echo "Raw output: $VERSION"
	if [ -z "${VERSION##*"no version"*}" ]; then
		echo "file not compiled with python";
		IS_PYTHON="false"
		cp "$file" "$OUTPUT_FILES"/temp-sort-files/bin-other
	else
		echo file compiled using "${VERSION#*': '*}"
		IS_PYTHON="true"
		cp "$file" "$OUTPUT_FILES"/temp-sort-files/bin-python
	fi

	echo { "\"raw_output"\": "\"$VERSION"\", "\"is_python"\": "\"$IS_PYTHON"\", "\"fileSHA256"\": "\"$hash"\", "\"fileName"\": "\"${file##*/}"\" } > output-files/identify-compiler/identify-compiler_"${file##*/}".json
    fi
done
ls -la "$OUTPUT_FILES"/output-files/identify-compiler/ -R
ls -la "$OUTPUT_FILES"/temp-sort-files/ -R
