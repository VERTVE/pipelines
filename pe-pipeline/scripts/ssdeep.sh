#!/bin/sh
ls -la

# Get fuzzy hash from source files
for file in sample-source/*; do
    if [ ! -d "$file" ]; then
        echo Analysing: "${file##*/}"
        hash=$(sha256sum "${file}" | cut -d ' ' -f1)

        mkdir -p output-files/ssdeep

	/ssdeep/ssdeep -b "$file" > output-files/ssdeep/ssdeep_"$hash".txt
    fi
done

# Concatenate hash file
cat output-files/ssdeep/ssdeep* > output-files/ssdeep/hashes.txt

# Compare hashes
/ssdeep/ssdeep -b -m output-files/ssdeep/hashes.txt sample-source/* |tee output-files/ssdeep/ssdeep_hashes_compared.txt


ls -la output-files/ssdeep/


# next: standardize-output.sh ??
