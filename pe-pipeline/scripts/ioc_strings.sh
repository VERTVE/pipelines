#!/bin/sh
OUTPUT_FILES=$(pwd)

loopFolders() {
	echo "In folder $1..."
	for file in "$1"/*; do
		folder=$1

		if [ ! -d "$file" ]; then
			# Ignore files that have same name as the folder (aka do not analyze .json file)
			if [[ ! "$file" == *"${folder##*/}"* ]]; then
				echo Analyze file: "$file in folder $folder"
				echo Running: iocstrings -t "$file"
				iocstrings -t "$file" > "$OUTPUT_FILES"/output-files/ioc_strings/"${folder##*/}"/ioc_strings_"${file##*/}".json
			fi
		fi

		if [ -d "$file" ]; then
			if [ ! -d "$OUTPUT_FILES"/output-files/ioc_strings/"${file##*/}" ]; then
				echo Create folder : "$OUTPUT_FILES"/output-files/ioc_strings/"${file##*/}"
				mkdir -p "$OUTPUT_FILES"/output-files/ioc_strings/"${file##*/}"
			fi
			loopFolders "$file"
		fi
	done
}

# Loop through all folders & files
loopFolders "results/binwalk"

echo List files
ls -R "$OUTPUT_FILES"/output-files/ioc_strings/
