#!/bin/sh

# Param 1: Git commit message
# Param 2: Determine if sample folder should be removed, param should
# exists and contain word 'rsample'

ls -R
git clone results update-git
cp -a output-files/* update-git/
cd update-git || exit
git checkout results

if [ -z "$2" ]; then
    echo "Sample folder not removed.";
else
    if [ "$2" = "rsample" ]; then
        rm -rf sample
        echo "Sample folder removed."
    fi;
fi;

# Add file _timestamp_ to be ignored in conflicts
echo _timestamp_ merge=ours > .gitattributes

git config merge.ours.driver true
git config user.email "cincan@cincan.io"
git config user.name "Cincan"
git add .
echo "Commit message is: "
echo "$1"
git commit -m "$1"
git pull --rebase
