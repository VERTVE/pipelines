#!/bin/bash
SAMPLESPATH=$(pwd)
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info
mkdir -p "$SAMPLESPATH"/output-files/json-output/jsunpackn

cd /jsunpack-n || echo "Changing path failed " | exit

# If folder is empty, do nothing. Otherwise loop through samples.
#if [[ -d $SAMPLESPATH/results/sample ]]; then
if [ ! -d "$SAMPLESPATH"/results/sample ] || [[ "$(find "$SAMPLESPATH"/results/sample |wc -l)" == 0 ]]; then
	echo "Folder is empty"
	date | tee -a "$SAMPLESPATH"/output-files/json-output/jsunpackn/jsunpackn-empty.json
else
	# Get version information
	echo "Found some samples to analyze"
	/usr/bin/python jsunpackn.py -h | grep version | head -n1 | sed -e 's/^[[:space:]]*//' > "$SAMPLESPATH"/output-files/tool-version-info/jsunpackn

	#` Loop through all files in the sample folder`
	for file in "$SAMPLESPATH"/results/sample/*; do
		# foldername for output files is hash of file
		hash=$(sha256sum "${file}" | cut -d ' ' -f1)
		xbase=${file##*/}
        xfext=${xbase##*.}
		xpref=${xbase%.*}
		echo "Processing ${xpref}.${xfext}"
		/usr/bin/python jsunpackn.py "$file" -d "$SAMPLESPATH"/output-files/shellcode/"$hash"/
		# | tee -a "$SAMPLESPATH"/output-files/jsunpack-n.log
		echo "output folder: $SAMPLESPATH/output-files/shellcode/${xpref}.${xfext}/"

		# Remove sample from work folder after analysis
		rm "$file"
		date >"$SAMPLESPATH"/output-files/_timestamp_
	done
fi
