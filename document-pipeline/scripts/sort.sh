#!/bin/sh
SAMPLESPATH=$(pwd)
#Install git
apt update && apt install -y git jq
git clone purge-git updated-results
git clone wait-samples updated-source

ls -lR

# Remove old samples from doc and pdf folders
rm "$SAMPLESPATH"/updated-source/doc/* ||:
rm "$SAMPLESPATH"/updated-source/pdf/* ||:

mkdir -p "$SAMPLESPATH"/updated-results/json-output/
cd "$SAMPLESPATH"/updated-source || exit

#echo "Replacing all whitespaces from filenames with underscore.."
# Remove all spaces from filenames, replace with underscore
# for f in *\ *; do mv "$f" "${f// /_}"; done
find "$SAMPLESPATH"/updated-source -depth -name "* *" -execdir rename 's/ /_/g' "{}" \;

if [ "$(find "$SAMPLESPATH"/updated-source | wc -l)" != 0 ]; then
    find "$SAMPLESPATH"/updated-source/* -maxdepth 0 -type f | while IFS= read -r sample; do
        # echo SHA256sum: "$(sha256sum "${sample}" | cut -d ' ' -f1)" | tee -a "$SAMPLESPATH"/updated-results/filetypes.log
        fileType=$(file "${sample}" --mime-type | awk '{print $2}' | cut -d "/" -f 2)
        hash="$(sha256sum "${sample}" | cut -d ' ' -f1)"

        if [ "${fileType}" != 'pdf' ]; then
            if [ ! -d "$SAMPLESPATH"/updated-source/doc ]; then mkdir "$SAMPLESPATH"/updated-source/doc; fi
            cp "${sample}" "$SAMPLESPATH"/updated-source/doc/
	else
            if [ ! -d "$SAMPLESPATH"/updated-source/pdf ]; then mkdir "$SAMPLESPATH"/updated-source/pdf; fi
            cp "${sample}" "$SAMPLESPATH"/updated-source/pdf/
        fi

        # echo "[\"$hash\", \"${sample##*/}\", \"$fileType\"]" | jq . || "Filename or type was not suitable for generating JSON, exiting.." | exit

        # Use jq instead of echo to sanitize input (hopefully!)
        jq -nc --arg fileSHA256 "$hash" \
              --arg fileName "${sample##*/}" \
              --arg fileType "$fileType" \
              '[ $fileSHA256, $fileName, $fileType ]'

               # Create JSON array of objects from generated output arrays in previous loop
    done | jq -s 'map( {fileSHA256: .[0], fileName: .[1], fileType: .[2] }) ' \
			> "$SAMPLESPATH"/updated-results/json-output/filetypes.json
    cat "$SAMPLESPATH"/updated-results/json-output/filetypes.json
    echo "Files processed"

    ls -l "$SAMPLESPATH"/updated-source/
else
    echo "No files to analyse"
fi

git config user.name "cincan"
git config user.email "cincan@cincan.io"
git add .
git commit -m "update source"
git pull

ls -l "$SAMPLESPATH"/updated-results/

cd "$SAMPLESPATH"/updated-results || echo "Directory change failed on committing changes" | exit
git config user.name "cincan"
git config user.email "cincan@cincan.io"
git add .
git commit -m "update log of sorted files"
git pull
find "$SAMPLESPATH/"updated-results/

