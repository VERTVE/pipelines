#!/bin/bash
OUTPUTPATH=$(pwd)/output-files
ORIGSAMPLESPATH=$(pwd)

apt-get update && apt-get install -y jq

mkdir -p "$OUTPUTPATH"/json-output/sctest
mkdir -p /tmp/

# Do nothing if folder is empty. Otherwise loop through samples.
if [[ "$(find "$ORIGSAMPLESPATH"/results/shellcode | wc -l)" == 0 ]]; then
	echo "Folder is empty"
	date >"$OUTPUTPATH"/sctest-empty.log
else
	for folder in "$ORIGSAMPLESPATH"/results/shellcode/*/; do
		# NOTE: foldername is sha256 hash of analysed file
		hash=$(basename "$folder")
		mkdir -p "$OUTPUTPATH"/json-output/sctest/"$hash"

		SAMPLESPATH=${folder%?}
		cd /peepdf || exit

		for file in "$SAMPLESPATH"/*; do
			xbase=${file##*/}

			echo "$(basename "$SAMPLESPATH")"/"$xbase" Results: >>"$OUTPUTPATH"/sctest.log
			/usr/bin/python peepdf.py "$file" -f --command="sctest file ${file}" > /tmp/sctest_"$xbase".log
			sc_data=$(cat /tmp/sctest_"$xbase".log)
			jq -n --arg hash "$hash" \
				  --arg file "$xbase" \
				  --arg shellcode "$sc_data" \
				  '{"originalFileSHA256": $hash, "shellCodeFileName": $file, "shellCode": $shellcode}' \
				  > "$OUTPUTPATH/json-output/sctest/$hash/sctest_$xbase.json"
				#    > "$SAMPLESPATH/output-files/json-output/strings/strings_$hash.json"
		done
	done

	cat "$OUTPUTPATH"/sctest.log
	rm -rf "$OUTPUTPATH"/sample
	date >"$OUTPUTPATH"/_timestamp_
fi
