#!/bin/bash
CREDS="credentials.yml"
CHECK="\e[32m[+]\e[0m"
ERROR="\e[0;91m[x]\e[0m"


## Check gitlab container is running

function checkGitlabState {
	echo "Checking that Gitlab container is up... (Ctrl+C to quit) "
	for run in {1..300}
	    do
	    GITLAB_STATE=$(docker inspect -f '{{ .State.Health.Status}}' gitlab.cincan.io)
	    if [ "${GITLAB_STATE}" == "healthy" ]; then
	        echo -e "\n${CHECK}" Gitlab is "$GITLAB_STATE"
		break
	    else
		echo -en "\r Gitlab is" "$GITLAB_STATE" "$run"
		if [ "$run" -lt 300 ]; then sleep 1; else echo "Failed";fi
	    fi;
	done
}


## Get Concourse external url from concourse.env, and gitlab external url from gitlab.rb

function getExternalUrls {
        GITLAB_EXTERNAL_URL=$(grep -r 'external_url' "${ENVIRONMENT_PATH}"/gitlab/config/gitlab.rb | sed -e 's|[;'\'']||g; s/.* //; s/https:\/\///; s/\:.*//')
        GITLAB_PORT=$(grep -r 'external_url' "${ENVIRONMENT_PATH}"/gitlab/config/gitlab.rb | sed -e 's|[;'\'']||g; s/.* //; s/https:\/\///; s/.*\://')
        echo "Using Gitlab external url: $GITLAB_EXTERNAL_URL:$GITLAB_PORT"

        CONCOURSE_EXTERNAL_URL=$(grep -r 'CONCOURSE_EXTERNAL_URL' "${ENVIRONMENT_PATH}"/build/concourse.env | sed 's/.*=//')
        echo "Using Concourse external url: $CONCOURSE_EXTERNAL_URL"
}


## Get certificates

function getCertificates {
 	REGISTRY_CRT=$(grep -r 'REGISTRY_HTTP_TLS_CERTIFICATE' "${ENVIRONMENT_PATH}"/build/concourse.env | sed 's/.*=//' | awk -F ":" '{print $1}' | awk -F "/" '{print $NF}')
	CONCOURSE_CRT=$(grep -r 'CONCOURSE_TLS_CERT' "${ENVIRONMENT_PATH}"/build/concourse.env | sed 's/.*=//' | awk -F ":" '{print $1}' | awk -F "/" '{print $NF}')

        echo -e "Using certificates:\n${CONCOURSE_CRT}\n${REGISTRY_CRT}"

        if [ -f "${ENVIRONMENT_PATH}"/certs/gitlab.cincan.io.crt ]; then
                GITLAB_CRT=gitlab.cincan.io.crt
                echo -e "${GITLAB_CRT}"
        else
                echo -e "Gitlab default certificate 'gitlab.cincan.io.crt' was not found,"
                read -r -p "type in certificate name ${ENVIRONMENT_PATH}/certs/" GITLAB_CRT

                if [ ! -f "${ENVIRONMENT_PATH}"/certs/"${GITLAB_CRT}" ]; then
                        echo -e "${ERROR} crt file not found!"
                        getCertificates
                fi
        fi
}


## Create a personal access token, if it was not given as an argument

function createToken() {
	if [ "$1" ]; then
		TOKEN=$1
	else
		# Create a token
	        echo -e "$CHECK" "Creating a personal access token"
	        TOKEN=$(docker exec gitlab.cincan.io gitlab-rails runner -e production "\
	                user = User.where(id: 1).first; \
	                pat = PersonalAccessToken.new(user_id: user.id, name: 'api key', scopes: Gitlab::Auth::API_SCOPES, expires_at: 1.day.from_now); \
	                pat.save!; \
	                user.personal_access_tokens << pat; \
	                puts pat.token;")
	fi

        echo Using token: "$TOKEN"
}


## Credentials

function modifyCredentialsFiles {
	# Get credentials from concourse.env file
	HANDLE=$(grep -r 'CONCOURSE_ADD_LOCAL_USER' "$ENVIRONMENT_PATH"/build/concourse.env | sed 's/.*=//' | awk -F ":" '{print $1}')
	PASS=$(grep -r 'CONCOURSE_ADD_LOCAL_USER' "$ENVIRONMENT_PATH"/build/concourse.env | sed 's/.*=//' | awk -F ":" '{print $2}')

	# Modify credentials.yml with correct Gitlab repository and personal access token
	echo -e "$CHECK" "2. Modifying $ENVIRONMENT_PATH/pipelines/credentials.yml"
	[ ! -d "$ENVIRONMENT_PATH"/pipelines ] && mkdir "$ENVIRONMENT_PATH"/pipelines
	cp ./{pipeline.yml,pipeline-private-registry.yml,"$CREDS"} "$ENVIRONMENT_PATH"/pipelines/
	sed -i -e "s#git@<GITLAB>:<USER>/<REPOSITORY>#git@$GITLAB_EXTERNAL_URL:root/$PIPELINE.git#" "$ENVIRONMENT_PATH"/pipelines/"$CREDS"

	# Add SSH key for Gitlab
	echo "  private-key: |" >> "$ENVIRONMENT_PATH"/pipelines/"$CREDS"
	cat "$ENVIRONMENT_PATH"/keys/cincan_rsa >> "$ENVIRONMENT_PATH"/pipelines/"$CREDS"

	# Count lines, indent SSH key
	ENDLINE=$(awk 'END{print NR}' "$ENVIRONMENT_PATH/pipelines/$CREDS")
	sed -i ""4","$ENDLINE"s/^/$(echo "    ")/" "$ENVIRONMENT_PATH"/pipelines/"$CREDS"

	# Create pipeline/credentials-private-registry.yml
	PRIVREG_CREDS=$ENVIRONMENT_PATH/pipelines/$(echo "$CREDS" | awk '{split($0,a,"."); print a[1]}')-private-registry.yml
	cp "$ENVIRONMENT_PATH"/pipelines/"$CREDS" "$PRIVREG_CREDS"

	# Add username and password to credentials-privreg.yml
	echo "  username: $HANDLE" >> "$PRIVREG_CREDS"
	echo "  password: $PASS" >> "$PRIVREG_CREDS"

	# Add certificate for private registry
	echo "  cert: |" >> "$PRIVREG_CREDS"
	cat "$ENVIRONMENT_PATH"/certs/$REGISTRY_CRT >> "$PRIVREG_CREDS"

	# Count lines, indent certificate from line number 6 until line number $ENDLINE
	STARTLINE=$(cat "$PRIVREG_CREDS" |sed -n '/END OPENSSH PRIVATE/=')
	ENDLINE=$(awk 'END{print NR}' "$PRIVREG_CREDS")
	echo "$((STARTLINE+4))" "$ENDLINE"
	sed -i ""$((STARTLINE+4))","$ENDLINE"s/^/$(echo "    ")/" "$PRIVREG_CREDS"
}


## Create repositories to the local gitlab

function createGitRepositories {
	## Copy SSH key to Gitlab using the token
    echo "Copy SSH key to Gitlab using token"
    
    #wget https://localhost:5443 --ca-certificate="$ENVIRONMENT_PATH"/certs/"$GITLAB_CRT"
    #wget https://172.20.0.5:5443 --no-check-certificate
    #curl -v https://172.20.0.5:5443/api/v4/users?private_token="$TOKEN" --cacert "$ENVIRONMENT_PATH"/certs/"$GITLAB_CRT"
    
    #docker exec gitlab.cincan.io curl -v -d '{"title":"CinCanKey","key":"'"$(cat "$ENVIRONMENT_PATH"/keys/cincan_rsa.pub)"'"}' -H 'Content-Type: application/json' https://"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/api/v4/user/keys?private_token="$TOKEN" --cacert "$ENVIRONMENT_PATH"/certs/"$GITLAB_CRT" --capath "$ENVIRONMENT_PATH"/certs/

	#curl -v -d '{"title":"CinCanKey","key":"'"$(cat "$ENVIRONMENT_PATH"/keys/cincan_rsa.pub)"'"}' -H 'Content-Type: application/json' https://"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/api/v4/user/keys?private_token="$TOKEN" --cacert "$ENVIRONMENT_PATH"/certs/"$GITLAB_CRT"  --key "$ENVIRONMENT_PATH"/certs/gitlab/gitlab.cincan.io.key --connect-timeout 60 --max-time 120
    
    
    FILE="$ENVIRONMENT_PATH"/keys/cincan_rsa.pub
    if test -f "$FILE"; then
        echo "$FILE exists."
    fi
    FILE="$ENVIRONMENT_PATH"/certs/gitlab.cincan.io.key
    if test -f "$FILE"; then
        echo "$FILE exists."
    fi
    FILE="$ENVIRONMENT_PATH"/certs/"$GITLAB_CRT"
    if test -f "$FILE"; then
        echo "$FILE exists."
    fi

    
    docker exec gitlab.cincan.io curl -d '{"title":"CinCanKey","key":"'"$(cat "$ENVIRONMENT_PATH"/keys/cincan_rsa.pub)"'"}' -H 'Content-Type: application/json' https://"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/api/v4/user/keys?private_token="$TOKEN" --cacert /etc/ssl/certs/gitlab/"$GITLAB_CRT" --key /etc/ssl/certs/gitlab/gitlab.cincan.io.key --connect-timeout 60 --max-time 120

	echo -e "$CHECK Copied CinCan SSH key to local Gitlab.\n$CHECK 3. Creating pipeline repositories to local Gitlab"

	# Create a project with Gitlab container's curl command

	docker exec gitlab.cincan.io curl --header "PRIVATE-TOKEN: $TOKEN" -X POST "https://$GITLAB_EXTERNAL_URL:$GITLAB_PORT/api/v4/projects?name=$PIPELINE" --cacert /etc/ssl/certs/gitlab/"$GITLAB_CRT" --key /etc/ssl/certs/gitlab/gitlab.cincan.io.key

	# Check if curl succeeded

	if [ $? -ne 0 ]; then echo "Git project creation failed." && exit 1;fi

	# Clone the repository, add files to master branch, create results and sample-source branches. (Repository is cloned to local folder 'master')

 echo "here we are"
 docker exec gitlab.cincan.io cat /etc/ssl/certs/gitlab/"$GITLAB_CRT"
 docker exec gitlab.cincan.io cat /etc/ssl/certs/gitlab/gitlab.cincan.io.key
 here "here again"
 cat "$ENVIRONMENT_PATH/certs/$GITLAB_CRT" 
 cat "$ENVIRONMENT_PATH/certs/gitlab.cincan.io.key"
 
 echo http.sslCAPath="$ENVIRONMENT_PATH/certs"
 ls "$ENVIRONMENT_PATH"/certs
 echo http.sslCAInfo="$ENVIRONMENT_PATH/certs/$GITLAB_CRT" 
 echo https://oauth2:"$TOKEN"@"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/root/"$PIPELINE".git
 echo git clone -c http.sslCAPath="$ENVIRONMENT_PATH/certs" -c http.sslCAInfo="$ENVIRONMENT_PATH/certs/$GITLAB_CRT" -c http.sslVerify=1 https://oauth2:"$TOKEN"@"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/root/"$PIPELINE".git ./master
	
		

	if [ ! -d master ]; then
		git clone -c http.sslCAPath="$ENVIRONMENT_PATH/certs" -c http.sslCAInfo="$ENVIRONMENT_PATH/certs/$GITLAB_CRT" -c http.sslVerify=1 https://oauth2:"$TOKEN"@"$GITLAB_EXTERNAL_URL":"$GITLAB_PORT"/root/"$PIPELINE".git ./master
		cd master/ || exit
	else
		cd master/ || exit
		git pull
	fi

	# Get branch names, and folders from pipeline's setup.json

	i=1
	while [ "$i" -lt $(($(grep -c 'branch_name' ../setup.json)+1)) ];
	do
	        BRANCH_NAME=$(grep 'branch_name' ../setup.json |awk -F':' 'NR==n {print $2}' n="$i" |tr -d "\", ")
	        echo "Create branch: $BRANCH_NAME"

	        git checkout -b "$BRANCH_NAME"
		rm -rf ./*

	        COPY_FILES_FROM_FOLDER=$(awk '/copy_files_from/{print $2}' ../setup.json |sed -n "$i"p |tr -d "\"")
	        if [ "$COPY_FILES_FROM_FOLDER" ]; then
	                echo "Copying files from $COPY_FILES_FROM_FOLDER to the $BRANCH_NAME branch"
	                cp -r ../"$COPY_FILES_FROM_FOLDER"/* .
	        fi


	        j=1
	        while [ "$j" -lt $(($(awk '/create_folders/{print $2}' ../setup.json |sed -n "$i"p | tr ',' ' ' |wc -w)+1)) ];
	        do
	                CREATE_FOLDER=$(awk '/create_folders/{print $2}' ../setup.json |sed -n "$i"p |tr -d "\"[]" |tr ',' '\n' |sed -n "$j"p)
	                if [ "$CREATE_FOLDER" ]; then
	                        [ ! -d "$CREATE_FOLDER" ] && mkdir "$CREATE_FOLDER"
	                        touch "$CREATE_FOLDER/.gitkeep"
	                fi
	                j=$((j+1))
	        done

		# Set samples branch to variable, to be used when initializing add2git-lfs

	        BRANCH_FUNCTION=$(awk '/branch_function/{print $2}' ../setup.json |sed -n "$i"p |tr -d "\",")
	        if [ "$BRANCH_FUNCTION" = "sample-source" ]; then
	                BRANCH_SAMPLESOURCE="$BRANCH_NAME"
	                echo "Branch $BRANCH_NAME is used for samples."
	        fi

	        git config user.name "Cincan"
	        git config user.email "cincan@cincan.io"
	        git add .
	        git commit -m "Add files to the $BRANCH_NAME branch"
	        git push origin "$BRANCH_NAME"

        	i=$((i+1))
	done
}


## Run Concourse commands

function initPipeline {
	# Login

	echo -e "$CHECK" "4. Login to Concourse and setup the pipeline"
	docker exec concourse.cincan.io fly -t local login --ca-cert "$ENVIRONMENT_PATH"/certs/"$CONCOURSE_CRT" -c "$CONCOURSE_EXTERNAL_URL" -u "$HANDLE" -p "$PASS" || { echo -e "$ERROR" "Login failed, see PIPELINE-README" ; exit 1; }

	# Set up the pipeline (regular / private registry version depending on arguments)
	if [[ "$1" == *"Private"* ]]; then
		PIPELINE_YML="pipeline-private-registry.yml"
		CREDENTIALS_YML="credentials-private-registry.yml"
	else
		PIPELINE_YML="pipeline.yml"
		CREDENTIALS_YML="credentials.yml"
	fi

	## Set up the pipeline

	echo -e "docker exec -i concourse.cincan.io fly -t local set-pipeline -p $PIPELINE -c $ENVIRONMENT_PATH/pipelines/$PIPELINE_YML -l $ENVIRONMENT_PATH/pipelines/$CREDENTIALS_YML"
	docker exec -i concourse.cincan.io fly -t local set-pipeline -p "$PIPELINE" -c "$ENVIRONMENT_PATH"/pipelines/"$PIPELINE_YML" -l "$ENVIRONMENT_PATH"/pipelines/"$CREDENTIALS_YML" -n
	echo -e "$CHECK" "Pipeline set up. Login to $CONCOURSE_EXTERNAL_URL"
	#echo -e "docker exec -it concourse.cincan.io fly -t local unpause-pipeline -p $PIPELINE"
	#docker exec -it concourse.cincan.io fly -t local unpause-pipeline -p "$PIPELINE"
}


## Create instructions file

function createInstructions {
	cat > "$ENVIRONMENT_PATH"/build/PIPELINE-README << EOL
Login to concourse:
docker exec -it concourse.cincan.io fly -t local login --ca-cert $ENVIRONMENT_PATH/certs/$CONCOURSE_CRT -c "$CONCOURSE_EXTERNAL_URL" -u <USERNAME> -p <PASSWORD>

Setup the pipeline:
docker exec -it concourse.cincan.io fly -t local set-pipeline -p $PIPELINE -c "$ENVIRONMENT_PATH"/pipelines/pipeline.yml -l $ENVIRONMENT_PATH/pipelines/credentials.yml

Unpause the pipeline from command line:
docker exec -it concourse.cincan.io fly -t local unpause-pipeline -p $PIPELINE

Destroy the pipeline:
docker exec -it concourse.cincan.io fly -t local destroy-pipeline -p $PIPELINE
EOL
}


## Add2git-lfs service

function initAdd2Git {
	echo -e "$CHECK" "5. Run add2git-lfs web user interface"
	docker pull cincan/add2git-lfs:latest
	docker run -d --rm --name add2git.cincan.io -v "$ENVIRONMENT_PATH"/certs:"$ENVIRONMENT_PATH"/certs -v "$(pwd)":/samples --net build_cincan --ip 172.20.0.7 -p 12358:12358 cincan/add2git-lfs:latest -branch "$BRANCH_SAMPLESOURCE" -folder . -user cincan -email cincan@cincan.io
}


## Script usage

function usage {
	echo -e "Arguments:\n-p <PIPELINE> (required)\n-e <ENVIRONMENT PATH> (required)\n-t <TOKEN> (optional)" && exit 0;
}



## Get arguments
while getopts e:p:t: option
do
	case "${option}"
	in
	e) ENVIRONMENT_PATH=${OPTARG};;
	p) PIPELINE=${OPTARG};;
	t) TOKEN=${OPTARG};;
	*) usage;;
	esac
done


if [ -z "$ENVIRONMENT_PATH" ] || [ -z "$PIPELINE" ]; then
	usage && exit 1
else
	echo "Setting up the pipeline to path: " "$ENVIRONMENT_PATH"
fi


cd "$PIPELINE" || exit

## Run functions

getExternalUrls
checkGitlabState
getCertificates
createToken "$TOKEN"
modifyCredentialsFiles
createGitRepositories
initPipeline "$@"
createInstructions
#initAdd2Git - has currently problems with LFS...
#echo -e "$CHECK" "Web interface is set up at 172.20.0.7:12358 (experimental)"

echo "See the README file, and the PIPELINE-README for more information."


